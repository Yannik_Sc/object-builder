use serde_yaml::{Mapping, Value};

///
/// Converts a string into a YAML value.
///
pub fn to_value(input: &String) -> Value {
    serde_yaml::from_str(input.as_str()).unwrap_or(Value::Null)
}

///
/// Converts the given string to a sequence of parsed values
///
pub fn to_sequence(input: Vec<String>) -> Value {
    input.iter().map(to_value).collect()
}

///
/// Parses the strings to mapping pairs split by `=` signs
///
pub fn to_mapping(input: Vec<String>) -> Value {
    let mut mapping = Mapping::new();

    for item in input {
        let mut split = item.split("=");
        let key = if let Some(key) = split.next() {
            key
        } else {
            break;
        };

        let value = split
            .fold(String::new(), |a, b| a + "=" + b)
            .chars()
            .skip(1)
            .collect::<String>();

        let value = if item.contains("=") && value.is_empty() {
            Value::String(String::new())
        } else {
            serde_yaml::from_str(value.as_str()).unwrap_or(Value::Null)
        };

        mapping.insert(Value::String(key.to_string()), value);
    }

    Value::Mapping(mapping)
}

#[cfg(test)]
mod tests {
    use crate::conversion::to_value;
    use crate::{to_mapping, to_sequence};
    use paste::paste;

    macro_rules! check_conversion_type {
        ($check_fn: ident, $($value: expr),*) => {
            paste! {
                #[test]
                fn [<test_to_value_types_$check_fn>]() {
                    $(
                        assert!(to_value(&String::from($value)).$check_fn());
                    )*
                }
            }
        };
    }

    check_conversion_type!(is_string, "hello world", "542.test");
    check_conversion_type!(is_bool, "true", "false");
    check_conversion_type!(is_u64, "312");
    check_conversion_type!(is_i64, "-541");
    check_conversion_type!(is_null, "~", "");
    check_conversion_type!(is_mapping, "{}");
    check_conversion_type!(is_sequence, "[]");
    check_conversion_type!(is_f64, "542.65");

    #[test]
    fn test_to_sequence() {
        let sequence = to_sequence(vec![String::new(), String::new(), String::new()]);

        assert!(sequence.is_sequence());
        assert_eq!(3, sequence.as_sequence().unwrap().len());
    }

    #[test]
    fn test_to_mapping() {
        let mapping = to_mapping(vec![
            String::from("test=value"),
            String::from("test2=value"),
            String::from("test3=value"),
        ]);

        assert!(mapping.is_mapping());
        assert_eq!(3, mapping.as_mapping().unwrap().len());
    }

    #[test]
    fn test_to_mapping_overrides() {
        let mapping = to_mapping(vec![
            String::from("test=value"),
            String::from("test=value"),
            String::from("test=value"),
        ]);

        assert!(mapping.is_mapping());
        assert_eq!(1, mapping.as_mapping().unwrap().len());
    }
}
