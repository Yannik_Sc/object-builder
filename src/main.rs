mod conversion;

use crate::conversion::{to_mapping, to_sequence};
use clap::{ArgEnum, Parser};
use serde::Serialize;
use serde_yaml::Value;
use std::process::exit;

#[derive(ArgEnum, Clone, Copy, Debug)]
enum OutputType {
    Toml,
    Yaml,
    Json,
}

#[derive(Parser, Debug)]
#[clap(version = env ! ("CARGO_PKG_VERSION"))]
struct Args {
    #[clap(short, long, help = "Build an array")]
    array: bool,

    #[clap(arg_enum, short, long, default_value_t = OutputType::Json, help = "Sets the output format")]
    output_type: OutputType,

    #[clap(short, long, help = "accept yaml/json input from stdin")]
    stdin: bool,

    values: Vec<String>,
}

impl OutputType {
    pub fn stringify_value<V: Serialize>(
        &self,
        value: V,
    ) -> Result<String, Box<dyn std::error::Error>> {
        Ok(match self {
            OutputType::Toml => toml::to_string(&value)?,
            OutputType::Yaml => serde_yaml::to_string(&value)?,
            OutputType::Json => serde_json::to_string(&value)?,
        })
    }
}

///
/// Checks if none of the given values contains an `=` sign, which would cause a map with keys but
/// without values, so we expect, that the user wants an array
///
fn array_implied(values: &Vec<String>) -> bool {
    for value in values {
        if value.contains("=") {
            return false;
        }
    }

    true
}

fn from_stdin() -> Option<Value> {
    serde_yaml::from_reader(std::io::stdin())
        .map(|value| Some(value))
        .unwrap_or_else(|error| {
            eprintln!("Could not parse input from stdin: {}", error);

            None
        })
}

fn output<V: Serialize>(value: V, args: &Args) {
    match args.output_type.stringify_value(value) {
        Ok(value) => println!("{}", value),
        Err(error) => {
            eprintln!("Could not generate output: {}", error);

            exit(1);
        }
    }
}

fn main() {
    let args = <Args as Parser>::parse();

    if args.stdin {
        let value = from_stdin();

        output(value.clone().unwrap_or_default(), &args);
        std::process::exit(value.map_or(1, |_| 0));
    }

    let value = if args.array || array_implied(&args.values) {
        to_sequence(args.values.clone())
    } else {
        to_mapping(args.values.clone())
    };

    output(value, &args);
}

#[cfg(test)]
mod tests {
    use crate::array_implied;

    #[test]
    fn imply_array() {
        assert!(array_implied(&vec![
            String::from("test"),
            String::from("test2"),
            String::from("test3"),
        ]));
        assert!(!array_implied(&vec![
            String::from("test"),
            String::from("test2="),
            String::from("test3"),
        ]));
    }
}
