![https://crates.io/crates/object-builder](https://img.shields.io/crates/v/object-builder.svg)

# Object builder

Build objects from your console! This tool allows you to create JSON, YAML, TOML (and potentially more) formatted
strings from console.

## Installation

### From crates.io

```bash
cargo install object-builder
```

### From source

```bash
git clone https://gitlab.com/Yannik_Sc/object-builder.git
cd object-builder
cargo install --path .
```

## Example

### Basics

After its installation the Object Builder can be called using the `ob` command.

```bash
ob test=true
# Results in:
# {"test": true}

# The same is possible with yaml
ob test=true -o yaml
# test: true

# or toml
ob test=true -o toml
# test = true
```

Currently Yaml, Toml and Json are supported as output format. Json is used as default though. In future there might be
more formats. For the most up-to-date list of formats check `ob --help`.

### Nested calls

To make more complex structures you can use the output of `ob` as an input for it
using [command substitution](http://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Command-Substitution).

```bash
ob value="$(ob -a some array)"
# {"value":["some","array"]}
```

The same, of cause, can be done with toml and yaml as output format. This however should only be set on the most
outer `ob` call otherwise the input value may result in an error.

### Array implication

`ob` generates an array when you pass the `-a` flag. This however can be implied as well when none of the inputs contain
an equal sign `=`.

```bash
ob value1 value2 value3
# ["value1","value2","value3"]

ob value1 value2= value3
# {"value1":null,"value2":"","value3":null}
```

Passing an `=` without value will result in an empty string for the given key. All keys that don't even contain the sign
will be assumed `null`.

### Converting with stdin

Since 1.1.0 the object builder can get yaml and json strings piped in to the stdin. Those files can be outputted/
effectively converted by this using a different `-o` output type.

```bash
# Input: {"test" true}
cat my.json | ob -o yaml
# Output: test: true
```

